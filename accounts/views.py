from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializer import *
from .emails import *
# Create your views here.
# https://www.youtube.com/watch?v=_-U8QAhFmTk


class RegisterAPI(APIView):

    def post(self, request):
        try:
            data = request.data
            serializer = UserSerializer(data=data)

            if serializer.is_valid():
                serializer.save()
                send_otp_via_email(data['email'])

                return Response({
                    "status": 200,
                    "message": "registration successfully check email",
                    "data": serializer.data
                    })
            
            return Response({
                "status": 400,
                "message": "something went wrong",
                "data": serializer.errors
            })
        except Exception as ex:
            print(ex)


class VerifyOTP(APIView):
    """Verify OTP"""
    def post(self, request):
        try:
            data = request.data
            serializer = VerifyAccountSerializer(data=data)
           
            print(data)

            if serializer.is_valid():
                email = serializer.data['email']
                otp = serializer.data['otp']
                print(email)
                print(otp)

                user_obj = User.objects.filter(email=email)

                if not user_obj.exists():
                    return Response({
                                "status": 400,
                                "message": "something went wrong",
                                "data": serializer.errors
                            })    
                
                if user_obj[0].otp != otp:
                    return Response({
                                "status": 400,
                                "message": "something went wrong",
                                "data": serializer.errors
                            })

                user_obj[0].is_verified = True
                user_obj[0].save()
                return Response({
                    "status": 200,
                    "message": "OTP verified successfully",
                    "data": serializer.data
                })
            
        except Exception as ex:
            print(ex)