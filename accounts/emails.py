from django.core.mail import send_mail
import random
from django.conf import settings
from .models import User


def send_otp_via_email(email):
    subject = 'Your account verification email'
    otp = random.randint(1000, 9999)
    message = f"Your otp is {otp}"
    #from_email = 'notification@datamaking.in'
    from_email = settings.EMAIL_HOST_USER

    try:
        send_mail(subject=subject, 
                message=message, 
                from_email=from_email, 
                recipient_list=[email])
    except Exception as ex:
        print(ex)
    
    user_obj = User.objects.get(email=email)
    user_obj.otp = otp
    user_obj.save()
