C
completeauth
Project ID: 49106326 
Invite your team
Add members to this project and start collaborating with your team.

The repository for this project is empty
You can get started by cloning the repository or start adding files to it with one of the following options.

Command line instructions
You can also upload existing files from your computer using the instructions below.

Git global setup
git config --global user.name "Pari Margu"
git config --global user.email "pari.margu2020@gmail.com"
Create a new repository
git clone https://gitlab.com/data-making/completeauth.git
cd completeauth
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main
Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/data-making/completeauth.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/data-making/completeauth.git
git push --set-upstream origin --all
git push --set-upstream origin --tags